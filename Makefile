.PHONY: install

include ./config/make/variables.mk

default: help

hyper-settings:
	cp ./hyper/.hyper.js ~/.hyper.js

## Installs everything on a clean system
install:
	bin/install

## Creates and updates symlinks to configuration files
symlinks:
	bin/symlink

## Updates all configurations
update: symlinks

# At the end so they don't run when running other targets
include ./config/make/help-target.mk
