# linux-config

Installation script and configurations for my personal Linux installation.

Almost everything is contained within this repo and symlinked to it's required location.

## Installation
Before being able to run the installation, a few manual steps have to be done.

In order:

1. `sudo apt update`
1. `sudo apt install git`
1. `git clone git@bitbucket.org:GuyllaumeCardinal/linux-config.git ~/dev/guyllaume/linux-config`
1. `cd ~/dev/guyllaume/linux-config/`
1. Once the above is done, you can run `make install`

### About The Install Script
I'm automating installation of software that can't be installed via the package manager, but I'm honestly not sure how
good or bad of an idea it is. I don't install my whole machine every day, so by the time I reinstall, I'm guessing most
of it will be outdated. We'll see I guess?

Still, versions are all configurable, but I feel like next time you run this, you'll be better off just doing it
manually. Maybe just create a list of software you use, but can't automate so at least you don't forget stuff.

## Other Tasks
Everything is in the Makefile. Running `make` will show you what is available.

## Things That Can't Be Automated

- Jetbrains Toolbox, PHPStorm, IntellijIDEA
    - Well it can, but it would be semi useless
- DisplayLink's USB Hub Drivers for Linux
    - Can't be bothered to find the URL.
