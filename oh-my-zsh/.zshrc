# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export ZSH="/home/${USER}/.oh-my-zsh"

DEFAULT_USER=`whoami`

export PATH="/home/linuxbrew/.linuxbrew/bin:/home/${USER}/.npm-global/bin:/home/${USER}/.local/bin:${PATH}"

source $ZSH/oh-my-zsh.sh
source ~/.zplug/init.zsh

zplug "~/.oh-my-zsh/plugins/git", from:local
zplug "~/.oh-my-zsh/plugins/docker", from:local
zplug "MichaelAquilina/zsh-you-should-use"
zplug "zsh-users/zsh-autosuggestions"
zplug "zsh-users/zsh-syntax-highlighting", defer:2

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    echo "Some plugins were not installed. Installing now."
    zplug install
fi

zplug load --verbose

source ~/.oh-my-zsh/custom/aliases.zsh
source ~/.oh-my-zsh/custom/environment-variables.zsh
source $(brew --prefix)/opt/powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
