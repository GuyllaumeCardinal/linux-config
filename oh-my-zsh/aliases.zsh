alias gpoh="g push origin HEAD"
alias gl="g pull"
alias tldr='docker run --rm -it -v ~/.tldr/:/root/.tldr/ nutellinoit/tldr'
alias top='gotop'

# Majisti
alias invoice="sh -c \"ssh -t bubba 'cd sites/invoicing.majisti.com/reporting-service/prod && bin/console app:generate:reports'\""
alias invoice-email="sh -c \"ssh -t bubba 'cd sites/invoicing.majisti.com/reporting-service/prod && bin/console app:email:reports'\""
alias invoice-display-entries="sh -c \"ssh -t bubba 'cd sites/invoicing.majisti.com/reporting-service/prod && bin/console app:toggl:display-entries'\"";
alias invoice-list-toggl-clients="sh -c \"ssh -t bubba 'cd sites/invoicing.majisti.com/reporting-service/prod && bin/console app:toggl:clients'\"";
alias invoice-list-quickbooks-clients="sh -c \"ssh -t bubba 'cd sites/invoicing.majisti.com/reporting-service/prod && bin/console app:quickbooks:list-customers'\"";
